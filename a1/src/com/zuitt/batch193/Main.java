package com.zuitt.batch193;

public class Main {

    public static void main(String[] args) {

        User Teacher = new User();
        Teacher.setFirstName("James");
        Teacher.setLastName("Doe");
        Teacher.setAge(30);
        Teacher.setAddress("123 Neverwhere Lane");

        Course subject = new Course();
        subject.setDescription("This is a course description");
        subject.setName("New Course");
        subject.setSeats(10);
        subject.setFee(99.5);
        subject.setInstructor(Teacher.getFirstName());

        //User
        System.out.println("User's First Name:");
        System.out.println(Teacher.getFirstName());
        System.out.println("User's Last Name:");
        System.out.println(Teacher.getLastName());
        System.out.println("User's Age:");
        System.out.println(Teacher.getAge());
        System.out.println("User's Address:");
        System.out.println(Teacher.getAddress());

        //Course
        System.out.println("Course Name:");
        System.out.println(subject.getName());
        System.out.println("Course Description:");
        System.out.println(subject.getDescription());
        System.out.println("Course Seats:");
        System.out.println(subject.getSeats());
        System.out.println("Course Fees:");
        System.out.println(subject.getFee());
        System.out.println("Course Instructor");
        System.out.println(subject.getInstructor());


    }
}
